# Webcounter

Simple Python Webcounter with redis server

## Build
docker build -t cfreire70/webcounter:1.0.1 .

## Dependencies
docker run -d  --name redis --rm redis:alpine

## Run
docker run -d --rm -p80:5000 --name webcounter --link redis -e REDIS_URL=redis cfreire70/webcounter:1.0.1

### Gitlab regisyer
gitlab-runner register -n \
--url https://gitlab.com/ \
--executor shell \
--description "docker-lab" \
--tag-list "production" \
--registration-token GR1348941jwzWt7B2rN2N3ydbPeuW